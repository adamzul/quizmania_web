<?php

namespace frontend\controllers;

use Yii;
use yii\web\Response;
use common\models\Soal;
use common\models\SoalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
/**
 * SoalController implements the CRUD actions for Soal model.
 */
class SoalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Soal models.
     * @return mixed
     */
    
    /**
     * Finds the Soal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Soal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Soal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionSoalBanyak()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Soal::find()->orderBy('rand()')->limit(7)->all();
        
        return $model;

    }
    public function actionSoal()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Soal::find()->orderBy('rand()')->one();
        
        return $model;

    }
}
