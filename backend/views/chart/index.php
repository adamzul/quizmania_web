<?php
/* @var $this yii\web\View */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\JenjangPendidikan;
use backend\assets\CustomAsset;
CustomAsset::register($this);
?>
<h1>chart</h1>

<div class="row">
    <div class="col s12 ">
      <div class="card white darken-1">
        <div class="card-content blue-text text-uppercase">
          <span class="card-title">jumlah siswa per provinsi</span>
          <div id="chart_provinsi">
          	<div class="progress" id="progress-provinsi">
		      <div class="indeterminate"></div>
		  </div>
          </div>
    	</div>
   
	  </div>
	</div>
</div>
<div class="row">
    <div class="col s12 ">
      <div class="card white darken-1">
        <div class="card-content red-text text-uppercase">
          <span class="card-title">jumlah siswa per kota</span>
          <div id="chart_kota"></div>
          <div class="progress" id="progress-kota">
		      <div class="indeterminate"></div>
		  </div>
    	</div>
   
	  </div>
	</div>
</div>

<div class="row">
    <div class="col s12 ">
      <div class="card white darken-1">
        <div class="card-content  text-uppercase">
          <span class="card-title green-text">rata - rata nilai permapel</span>
          
          	<?php echo Html::dropDownList('id_jenjang_pendidikan', null, ArrayHelper::map(JenjangPendidikan::find()->all(),'id_jenjang_pendidikan', 'jenjang'),['id' => 'dropdown-mapel', 'style' => 'display:block', 'class' => 'button']); ?>
          
          <div>
			
			<div id="chart_mapel"></div>
			<div class="progress" id="progress-permapel">
		      <div class="indeterminate"></div>
		  </div>
		</div>
    </div>
   
  </div>
</div>
</div>
<?php
// $this->registerJsFile('@web/js/call-chart-provinsi.js');
// $this->registerJsFile('@web/js/call-chart-kota.js');
// $this->registerJsFile('@web/js/call-chart-mapel.js');
?>