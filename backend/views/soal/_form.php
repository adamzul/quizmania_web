<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Soal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="soal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_mata_pelajaran')->textInput() ?>

    <?= $form->field($model, 'id_jenjang_pendidikan')->textInput() ?>

    <?= $form->field($model, 'kunci_jawaban')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'soal')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pilihan1')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pilihan2')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pilihan3')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'pilihan4')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
