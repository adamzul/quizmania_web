<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SoalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="soal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_soal') ?>

    <?= $form->field($model, 'id_mata_pelajaran') ?>

    <?= $form->field($model, 'id_jenjang_pendidikan') ?>

    <?= $form->field($model, 'kunci_jawaban') ?>

    <?= $form->field($model, 'soal') ?>

    <?php // echo $form->field($model, 'pilihan1') ?>

    <?php // echo $form->field($model, 'pilihan2') ?>

    <?php // echo $form->field($model, 'pilihan3') ?>

    <?php // echo $form->field($model, 'pilihan4') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
