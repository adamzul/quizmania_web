<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\EndUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'End Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="end-user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create End User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_user',
            'id_jenjang_pendidikan',
            [
            'attribute' => 'jenjang',
            'value' => 'idJenjangPendidikan.jenjang',
            ],
            [
            'attribute' => 'provinsi',
            'value' => 'idProvinsi.provinsi',
            ],
            [
            'attribute' => 'kota',
            'value' => 'idKota.kota',
            ],
            'name',
            'username',
            'email:email',
            // 'password_hash',
            // 'created_at',
            // 'updated_at',
            // 'status',
            'alamat:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
