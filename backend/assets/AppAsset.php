<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/materialize.min.css'
    ];
    public $js = [
    // 'js/call-chart-provinsi.js',
    // 'js/call-chart-kota.js',
    // 'js/call-chart-mapel.js',
    'js/combo-chart.js',
    'https://www.gstatic.com/charts/loader.js',
    'js/materialize.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
