<?php

namespace backend\controllers;
use Yii;
use common\models\Provinsi;
use common\models\Kota;
use common\models\EndUser;
use common\models\MataPelajaran;
use common\models\history;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
class ChartController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	
        return $this->render('index');
    }
    public function actionChartProvinsi()
    {
        
    	$arrayProvinsi = [];
    	$provinsis = Provinsi::find()->asArray()->all();
    	$endUsers = EndUser::find()->asArray()->all();
    	foreach ($provinsis as $provinsi) {
    		# code...
    		$arrayProvinsi[$provinsi['provinsi']] = 0;
    	}
    	
    		foreach ($endUsers as $EndUser) {
    			# code...
    			++$arrayProvinsi[Provinsi::find()->select(['provinsi'])->where(['id_provinsi' => $EndUser['id_provinsi']])->asArray()->one()['provinsi']];
    		}
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	return $arrayProvinsi;
    }

    public function actionChartKota()
    {
    	$arrayKota = [];
    	$kotas = Kota::find()->asArray()->all();
    	$endUsers = EndUser::find()->asArray()->all();
    	foreach ($kotas as $kota) {
    		# code...
    		$arrayKota[$kota['kota']] = 0;
    	}
    	
    		foreach ($endUsers as $EndUser) {
    			# code...
    			++$arrayKota[Kota::find()->select(['kota'])->where(['id_kota' => $EndUser['id_kota']])->asArray()->one()['kota']];
    		}
    	\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	return $arrayKota;
    }

    public function actionChartMapel()
    {
        $data = Yii::$app->request->post();
        $idJenjang = $data['idJenjang'];
        // $idJenjang = 12;
    	$mapels = MataPelajaran::find()->asArray()->all();
    	$histories = history::find()->where(['id_jenjang_pendidikan' => $idJenjang])->asArray()->all();
        $arrayMapel = [];
        foreach ($histories as $history) {
            # code...
            $arrayMapel[MataPelajaran::find()->select(['mata_pelajaran'])->where(['id_mata_pelajaran' => $history['id_mata_pelajaran']])->asArray()->one()['mata_pelajaran']] = 0;
        }
        foreach ($histories as $history) {
            # code...
            ++$arrayMapel[MataPelajaran::find()->select(['mata_pelajaran'])->where(['id_mata_pelajaran' => $history['id_mata_pelajaran']])->asArray()->one()['mata_pelajaran']];
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $arrayMapel;
    }

    // public function beforeAction($action) {
    //     $this->enableCsrfValidation = false;
    //     return parent::beforeAction($action);
    // }
}
