$(document).ready(function(){
	callChartMapel();
	$(document).on('change', '#dropdown-mapel', function(){
		callChartMapel();
	});
	
});

function callChartMapel()
{
	var idJenjang = $('#dropdown-mapel').val();
	if(idJenjang == '' || idJenjang == null)
		idJenjang = 1;
$.ajax({
	url: 'index.php?r=chart%2Fchart-mapel',
	type: 'post',
	data: {
		idJenjang: idJenjang,
		 _csrf : '<?=Yii::$app->request->getCsrfToken()?>'
	},success: function(arrayMapel)
	{
		var listData=[[''],['']];//nilai array tertentu dikosongkan
		  i = 1;
		  for(var k in arrayMapel) {
		    listData[0][i] = k;
		    listData[1][i] = arrayMapel[k];
		    i++;
		  }
		  $('#progress-permapel').hide();
		comboChart(listData, 'rata- rata nilai permapel', 'jumlah', null, 'chart_mapel');
	},error: function(arrayMapel)
	{
		alert('error');
		console.log(arrayMapel['responseText']);
	}
	

	});
}