function comboChart(arrayData, title, vAxis, hAxis, place){
  google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable(arrayData);

    var options = {
      title : title,
      vAxis: {title: vAxis},
      hAxis: {title: hAxis},
      seriesType: 'bars',
      series: {5: {type: ''}}
    };

    var chart = new google.visualization.ComboChart(document.getElementById(place));
    chart.draw(data, options);
  }
}