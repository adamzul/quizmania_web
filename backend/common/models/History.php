<?php

namespace app\common\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id_history
 * @property integer $id_user
 * @property integer $id_jenjang_pendidikan
 * @property integer $id_mata_pelajaran
 * @property double $nilai
 *
 * @property JenjangPendidikan $idJenjangPendidikan
 * @property MataPelajaran $idMataPelajaran
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'required'],
            [['id_user', 'id_jenjang_pendidikan', 'id_mata_pelajaran'], 'integer'],
            [['nilai'], 'number'],
            [['id_jenjang_pendidikan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']],
            [['id_mata_pelajaran'], 'exist', 'skipOnError' => true, 'targetClass' => MataPelajaran::className(), 'targetAttribute' => ['id_mata_pelajaran' => 'id_mata_pelajaran']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_history' => 'Id History',
            'id_user' => 'Id User',
            'id_jenjang_pendidikan' => 'Id Jenjang Pendidikan',
            'id_mata_pelajaran' => 'Id Mata Pelajaran',
            'nilai' => 'Nilai',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenjangPendidikan()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMataPelajaran()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mata_pelajaran' => 'id_mata_pelajaran']);
    }
}
