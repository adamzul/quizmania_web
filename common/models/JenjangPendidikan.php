<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenjang_pendidikan".
 *
 * @property integer $id_jenjang_pendidikan
 * @property integer $jenjang
 *
 * @property History[] $histories
 * @property Soal[] $soals
 * @property User[] $users
 */
class JenjangPendidikan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenjang_pendidikan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenjang'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_jenjang_pendidikan' => 'Id Jenjang Pendidikan',
            'jenjang' => 'Jenjang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSoals()
    {
        return $this->hasMany(Soal::className(), ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']);
    }
}
