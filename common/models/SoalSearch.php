<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Soal;

/**
 * SoalSearch represents the model behind the search form about `common\models\Soal`.
 */
class SoalSearch extends Soal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_soal', 'id_mata_pelajaran', 'id_jenjang_pendidikan'], 'integer'],
            [['kunci_jawaban', 'soal', 'pilihan1', 'pilihan2', 'pilihan3', 'pilihan4', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Soal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_soal' => $this->id_soal,
            'id_mata_pelajaran' => $this->id_mata_pelajaran,
            'id_jenjang_pendidikan' => $this->id_jenjang_pendidikan,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'kunci_jawaban', $this->kunci_jawaban])
            ->andFilterWhere(['like', 'soal', $this->soal])
            ->andFilterWhere(['like', 'pilihan1', $this->pilihan1])
            ->andFilterWhere(['like', 'pilihan2', $this->pilihan2])
            ->andFilterWhere(['like', 'pilihan3', $this->pilihan3])
            ->andFilterWhere(['like', 'pilihan4', $this->pilihan4]);

        return $dataProvider;
    }
}
