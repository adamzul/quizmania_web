<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\EndUser;

/**
 * EndUserSearch represents the model behind the search form about `common\models\EndUser`.
 */
class EndUserSearch extends EndUser
{
    public $jenjang, $provinsi, $kota;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        
        return [
            [['id_user', 'id_jenjang_pendidikan', 'id_provinsi', 'id_kota', 'status'], 'integer'],
            [['name', 'username', 'email', 'password_hash', 'created_at', 'updated_at', 'alamat', 'provinsi', 'kota', 'jenjang'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EndUser::find()->joinWith(['idProvinsi']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_user' => $this->id_user,
            'id_jenjang_pendidikan' => $this->id_jenjang_pendidikan,
            'id_provinsi' => $this->id_provinsi,
            'id_kota' => $this->id_kota,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'idJenjangPendidikan.jenjang', $this->jenjang])
            ->andFilterWhere(['like', 'idProvinsi.provinsi', $this->provinsi])
            ->andFilterWhere(['like', 'idKota.kota', $this->kota]);

        return $dataProvider;
    }
}
