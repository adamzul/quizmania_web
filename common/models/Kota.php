<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kota".
 *
 * @property integer $id_kota
 * @property integer $id_provinsi
 * @property string $kota
 *
 * @property Provinsi $idProvinsi
 * @property User[] $users
 */
class Kota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kota';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_provinsi'], 'integer'],
            [['kota'], 'string', 'max' => 30],
            [['id_provinsi'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['id_provinsi' => 'id_provinsi']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kota' => 'Id Kota',
            'id_provinsi' => 'Id Provinsi',
            'kota' => 'Kota',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvinsi()
    {
        return $this->hasOne(Provinsi::className(), ['id_provinsi' => 'id_provinsi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_kota' => 'id_kota']);
    }
}
