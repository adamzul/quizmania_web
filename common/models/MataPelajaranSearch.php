<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MataPelajaran;

/**
 * MataPelajaranSearch represents the model behind the search form about `common\models\MataPelajaran`.
 */
class MataPelajaranSearch extends MataPelajaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_mata_pelajaran'], 'integer'],
            [['mata_pelajaran'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MataPelajaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mata_pelajaran' => $this->id_mata_pelajaran,
        ]);

        $query->andFilterWhere(['like', 'mata_pelajaran', $this->mata_pelajaran]);

        return $dataProvider;
    }
}
