<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "soal".
 *
 * @property integer $id_soal
 * @property integer $id_mata_pelajaran
 * @property integer $id_jenjang_pendidikan
 * @property string $kunci_jawaban
 * @property string $soal
 * @property string $pilihan1
 * @property string $pilihan2
 * @property string $pilihan3
 * @property string $pilihan4
 * @property string $created_at
 *
 * @property MataPelajaran $idMataPelajaran
 * @property JenjangPendidikan $idJenjangPendidikan
 */
class Soal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'soal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_mata_pelajaran', 'id_jenjang_pendidikan'], 'integer'],
            [['soal', 'pilihan1', 'pilihan2', 'pilihan3', 'pilihan4'], 'string'],
            [['created_at'], 'safe'],
            [['kunci_jawaban'], 'string', 'max' => 1],
            [['id_mata_pelajaran'], 'exist', 'skipOnError' => true, 'targetClass' => MataPelajaran::className(), 'targetAttribute' => ['id_mata_pelajaran' => 'id_mata_pelajaran']],
            [['id_jenjang_pendidikan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_soal' => 'Id Soal',
            'id_mata_pelajaran' => 'Id Mata Pelajaran',
            'id_jenjang_pendidikan' => 'Id Jenjang Pendidikan',
            'kunci_jawaban' => 'Kunci Jawaban',
            'soal' => 'Soal',
            'pilihan1' => 'Pilihan1',
            'pilihan2' => 'Pilihan2',
            'pilihan3' => 'Pilihan3',
            'pilihan4' => 'Pilihan4',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMataPelajaran()
    {
        return $this->hasOne(MataPelajaran::className(), ['id_mata_pelajaran' => 'id_mata_pelajaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenjangPendidikan()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']);
    }
}
