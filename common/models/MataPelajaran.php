<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mata_pelajaran".
 *
 * @property integer $id_mata_pelajaran
 * @property string $mata_pelajaran
 *
 * @property History[] $histories
 * @property Soal[] $soals
 */
class MataPelajaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mata_pelajaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mata_pelajaran'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_mata_pelajaran' => 'Id Mata Pelajaran',
            'mata_pelajaran' => 'Mata Pelajaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['id_mata_pelajaran' => 'id_mata_pelajaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSoals()
    {
        return $this->hasMany(Soal::className(), ['id_mata_pelajaran' => 'id_mata_pelajaran']);
    }
}
