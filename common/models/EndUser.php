<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id_user
 * @property integer $id_jenjang_pendidikan
 * @property integer $id_provinsi
 * @property integer $id_kota
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property string $alamat
 *
 * @property JenjangPendidikan $idJenjangPendidikan
 * @property Provinsi $idProvinsi
 * @property Kota $idKota
 */
class EndUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_jenjang_pendidikan', 'id_provinsi', 'id_kota', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['alamat'], 'string'],
            [['name', 'username'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 40],
            [['password_hash'], 'string', 'max' => 60],
            [['id_jenjang_pendidikan'], 'exist', 'skipOnError' => true, 'targetClass' => JenjangPendidikan::className(), 'targetAttribute' => ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']],
            [['id_provinsi'], 'exist', 'skipOnError' => true, 'targetClass' => Provinsi::className(), 'targetAttribute' => ['id_provinsi' => 'id_provinsi']],
            [['id_kota'], 'exist', 'skipOnError' => true, 'targetClass' => Kota::className(), 'targetAttribute' => ['id_kota' => 'id_kota']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'id_jenjang_pendidikan' => 'Id Jenjang Pendidikan',
            'id_provinsi' => 'Id Provinsi',
            'id_kota' => 'Id Kota',
            'name' => 'Name',
            'username' => 'Username',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'alamat' => 'Alamat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdJenjangPendidikan()
    {
        return $this->hasOne(JenjangPendidikan::className(), ['id_jenjang_pendidikan' => 'id_jenjang_pendidikan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvinsi()
    {
        return $this->hasOne(Provinsi::className(), ['id_provinsi' => 'id_provinsi']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKota()
    {
        return $this->hasOne(Kota::className(), ['id_kota' => 'id_kota']);
    }
}
