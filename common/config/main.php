<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
	    'user' => [
    'identityClass' => 'common\models\User',
],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
